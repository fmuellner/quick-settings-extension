#!/usr/bin/bash

EXTRA_DATA=(
  dark-mode-symbolic.svg
)

EXTRA_SOURCES=(
  quickSettings.js 
  remoteAccess.js 
  status/
)

gnome-extensions pack \
  ${EXTRA_SOURCES[@]/#/--extra-source=} \
  ${EXTRA_DATA[@]/#/--extra-source=}
