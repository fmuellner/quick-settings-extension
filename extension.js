/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

/* exported init */

const GETTEXT_DOMAIN = 'my-indicator-extension';

const {GObject, St} = imports.gi;

const ExtensionUtils = imports.misc.extensionUtils;
const Main = imports.ui.main;
const PopupMenu = imports.ui.popupMenu;
const PanelMenu = imports.ui.panelMenu;

const _ = ExtensionUtils.gettext;
const Me = ExtensionUtils.getCurrentExtension();

const {QuickSettingsMenu, QuickMenuToggle} = Me.imports.quickSettings;

const N_COLUMNS = 2;

const QuickSettings = GObject.registerClass(
class QuickSettings extends PanelMenu.Button {
    constructor() {
        super(0.0, _('System'), true);

        this._indicators = new St.BoxLayout({
            style_class: 'panel-status-indicators-box',
        });
        this.add_child(this._indicators);

        this.setMenu(new QuickSettingsMenu(this, N_COLUMNS));

        this._system = new Me.imports.status.system.Indicator();
        this._volume = new Me.imports.status.volume.Indicator();
        this._brightness = new Me.imports.status.brightness.Indicator();
        this._nightLight = new Me.imports.status.nightLight.Indicator();
        this._network = new Me.imports.status.network.Indicator();
        this._bluetooth = new Me.imports.status.bluetooth.Indicator();
        this._darkMode = new Me.imports.status.darkMode.Indicator();
        this._powerProfiles = new Me.imports.status.powerProfiles.Indicator();
        this._rfkill = new Me.imports.status.rfkill.Indicator();
        this._autoRotate = new Me.imports.status.autoRotate.Indicator();

        this._indicators.add_child(this._brightness);
        this._indicators.add_child(this._nightLight);
        this._indicators.add_child(this._darkMode);
        this._indicators.add_child(this._powerProfiles);
        this._indicators.add_child(this._network);
        this._indicators.add_child(this._bluetooth);
        this._indicators.add_child(this._rfkill);
        this._indicators.add_child(this._autoRotate);
        this._indicators.add_child(this._volume);
        this._indicators.add_child(this._system)

        this._addItems(this._system.quickSettingsItems, N_COLUMNS);
        this._addItems(this._volume.quickSettingsItems, N_COLUMNS);
        this._addItems(this._brightness.quickSettingsItems, N_COLUMNS);

        this._addItems(this._network.quickSettingsItems);
        this._addItems(this._bluetooth.quickSettingsItems);
        this._addItems(this._nightLight.quickSettingsItems);
        this._addItems(this._darkMode.quickSettingsItems);
        this._addItems(this._powerProfiles.quickSettingsItems);
        this._addItems(this._rfkill.quickSettingsItems);
        this._addItems(this._autoRotate.quickSettingsItems);
    }

    _addItems(items, colSpan = 1) {
        items.forEach(item => this.menu.addItem(item, colSpan));
    }
});

class Extension {
    constructor() {
        ExtensionUtils.initTranslations(GETTEXT_DOMAIN);
    }

    enable() {
        this._remoteAccess = new Me.imports.remoteAccess.RemoteAccessApplet();
        Main.panel.addToStatusArea('remote-access', this._remoteAccess);

        const pos = Main.panel._rightBox.get_n_children();
        this._quickSettings = new QuickSettings();
        Main.panel.addToStatusArea('quickSettingsExtension', this._quickSettings, pos);

        Main.panel.statusArea.aggregateMenu?.hide();

        const {closeQuickSettings} = Main.panel;
        this._closeQuickSettings = closeQuickSettings;
        Main.panel.closeQuickSettings = function () {
            closeQuickSettings?.call(this);
            Main.panel.statusArea.quickSettingsExtension.menu.close();
        };
    }

    disable() {
        this._remoteAccess.destroy();
        this._quickSettings.destroy();

        Main.panel.statusArea.aggregateMenu?.show();

        if (this._closeQuickSettings)
            Main.panel.closeQuickSettings = this._closeQuickSettings;
        else
            delete Main.panel.closeQuickSettings;
    }
}

function init() {
    return new Extension();
}
